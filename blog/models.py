from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse, reverse_lazy
from django.utils import timezone


class Post(models.Model):
    author = models.ForeignKey(get_user_model())
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now
    )
    published_date = models.DateTimeField(
        blank=True, null=True
    )

    def __str__(self):
        return self.title

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def get_absolute_url(self):
        return reverse_lazy('post_detail', args=[self.pk, ])
